import { v4 as uuid } from "uuid";
/**
 *  All Dashboard Routes
 *
 *  Understanding name/value pairs for Dashboard routes
 *
 *  Applicable for main/root/level 1 routes
 *  icon 		: String - It's only for main menu or you can consider 1st level menu item to specify icon name.
 *
 *  Applicable for main/root/level 1 and subitems routes
 * 	id 			: Number - You can use uuid() as value to generate unique ID using uuid library, you can also assign constant unique ID for react dynamic objects.
 *  title 		: String - If menu contains childern use title to provide main menu name.
 *  badge 		: String - (Optional - Default - '') If you specify badge value it will be displayed beside the menu title or menu item.
 * 	badgecolor 	: String - (Optional - Default - 'primary' ) - Used to specify badge background color.
 *
 *  Applicable for subitems / children items routes
 *  name 		: String - If it's menu item in which you are specifiying link, use name ( don't use title for that )
 *  children	: Array - Use to specify submenu items
 *
 *  Used to segrigate menu groups
 *  grouptitle : Boolean - (Optional - Default - false ) If you want to group menu items you can use grouptitle = true,
 *  ( Use title : value to specify group title  e.g. COMPONENTS , DOCUMENTATION that we did here. )
 *
 */

export const DashboardMenu = [
    {
        id: uuid(),
        title: "Dashboard",
        icon: "home",
        link: "/",
    },
    {
        id: uuid(),
        title: "TRANSAKSI",
        grouptitle: true,
    },
    {
        id: uuid(),
        title: "Transaksi",
        icon: "layers",
        children: [
            { id: uuid(), link: "/pages/transaksi", name: "Transaksi" },
            { id: uuid(), link: "/pages/sender", name: "Sender (Outbox)" },
            { id: uuid(), link: "/pages/komplain", name: "Komplain" },
            { id: uuid(), link: "/pages/pesan-tambahan", name: "Pesan Tambahan" },
            { id: uuid(), link: "/pages/trace-inbox", name: "Trace Inbox" },
            { id: uuid(), link: "/pages/log-terminal", name: "Log Terminal" },
        ],
    },
    {
        id: uuid(),
        title: "Administrator",
        icon: "user",
        children: [
            { id: uuid(), link: "/pages/operator", name: "Operator" },
            { id: uuid(), link: "/pages/produk", name: "Produk" },
            { id: uuid(), link: "/pages/terminal", name: "Terminal" },
            { id: uuid(), link: "/pages/inventory", name: "Inventory" },
            { id: uuid(), link: "/pages/voucher-fisik", name: "Voucher Fisik" },
            { id: uuid(), link: "/pages/cluster", name: "Cluster" },
            { id: uuid(), link: "/pages/blacklist", name: "Blacklist" },
            { id: uuid(), link: "/pages/history-reseller", name: "History Reseller" },
            { id: uuid(), link: "/pages/reseller", name: "Reseller" },
            { id: uuid(), link: "/pages/user", name: "User" },
            { id: uuid(), link: "/pages/log-user", name: "Log User" },
        ],
    },
    {
        id: uuid(),
        title: "PENGATURAN & LAPORAN",
        grouptitle: true,
    },
    {
        id: uuid(),
        title: "Pengaturan",
        icon: "settings",
        children: [
            { id: uuid(), link: "/pages/format-request", name: "Format Request" },
            { id: uuid(), link: "/pages/format-balasan", name: "Format Balasan" },
            { id: uuid(), link: "/pages/utility", name: "Utility" },
            { id: uuid(), link: "/pages/option", name: "Options" },
        ],
    },
    {
        id: uuid(),
        title: "Laporan",
        icon: "monitor",
        children: [{ id: uuid(), link: "/pages/laporan-penjualan", name: "Laporan Penjualan" }],
    },
    // {
    //     id: uuid(),
    //     title: "Users",
    //     icon: "user",
    //     children: [
    //         { id: uuid(), link: "/authentication/sign-in", name: "Sign In" },
    //     ],
    // },
];

export default DashboardMenu;
