// import node module libraries
import React from "react";
import {
  Row,
  Col,
  Form,
  Button,
} from "react-bootstrap";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import "rsuite/dist/rsuite-no-reset.min.css";

const UtilitysBtn = () => {

  return (
    <>
      <fieldset class="border rounded-3 py-2 px-3 h-100">
        <legend class="float-none w-auto px-3 mb-0" >Hapus</legend>
        <div className="d-grid gap-2">
          <Button variant="btn btn-outline-white" size="lg">
            Reset Saldo Akun
          </Button>
          <Button variant="btn btn-outline-white" size="lg">
            Reset Poin Reseller
          </Button>
          <Button variant="btn btn-outline-white" size="lg">
            Reset Jurnal
          </Button>
          <Button variant="btn btn-outline-white" size="lg">
            Reset Inbox & Outbox
          </Button>
          <Button variant="btn btn-outline-white" size="lg">
            Hapus Transaksi Inbox
          </Button>
          <Button variant="btn btn-outline-white" size="lg">
            Hapus Transaksi Outbox
          </Button>
          <Button variant="btn btn-outline-white" size="lg">
            Hapus Popup Info
          </Button>
        </div>
      </fieldset>
    </>
  );
};

export default UtilitysBtn;
