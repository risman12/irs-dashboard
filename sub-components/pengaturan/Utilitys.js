// import node module libraries
import React from "react";
import {
  Row,
  Col,
  Form,
  Button,
} from "react-bootstrap";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import "rsuite/dist/rsuite-no-reset.min.css";

const Utilitys = () => {

  return (
    <>
      <fieldset class="border rounded-3 py-2 px-3 h-100">
        <legend class="float-none w-auto px-3 mb-0" >Auto Hapus Data</legend>
        <Form>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}>
              <Form.Label className="position-relative">Hapus data Transaksi lebih dari</Form.Label>
            </Col>
            <Col sm={8}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}>
              <Form.Label className="position-relative">Hapus Inbox Center Lebih dari</Form.Label>
            </Col>
            <Col sm={8}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}>
              <Form.Label className="position-relative">Hapus Sender Lebih dari</Form.Label>
            </Col>
            <Col sm={8}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}>
              <Form.Label className="position-relative">Hapus Inbox Operator Lebih dari</Form.Label>
            </Col>
            <Col sm={8}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}>
              <Form.Label className="position-relative">Hapus Mutasi Saldo RS Lebih dari</Form.Label>
            </Col>
            <Col sm={8}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}>
              <Form.Label className="position-relative">Hapus Transaksi Histori Lebih dari</Form.Label>
            </Col>
            <Col sm={8}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}>
              <Form.Label className="position-relative">Hapus Sender Histori Lebih dari</Form.Label>
            </Col>
            <Col sm={8}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={4}></Col>
            <Col sm={8}>
              <Button variant="primary">Simpan</Button>
            </Col>
          </Form.Group>
        </Form>
      </fieldset>
    </>
  );
};

export default Utilitys;
