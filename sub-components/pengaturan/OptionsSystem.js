// import node module libraries
import React from "react";
import {
  Row,
  Col,
  Form,
  Button,
  InputGroup,
} from "react-bootstrap";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import "rsuite/dist/rsuite-no-reset.min.css";
import { DateRangePicker } from "rsuite";

const OptionsSystem = () => {

  return (
    <>
      <fieldset class="border rounded-3 py-2 px-3 h-100">
        <legend class="float-none w-auto px-3 mb-0" >System</legend>
        <Form>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={6}>
              <Form.Label className="position-relative">Batasan Transaksi Tujuan/nomor sama</Form.Label>
            </Col>
            <Col sm={6}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={6}>
              <Form.Check
                type="checkbox"
                id="autoSizingCheck"
                className="mt-1"
                label="NonAktif Reseller jika lebih dari (Hari)"
              />
            </Col>
            <Col sm={6}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col column sm={6}>
              <Form.Check
                type="checkbox"
                id="autoSizingCheck"
                className="mt-1"
                label="Abaikan Inbox lebih dari (Jam)"
              />
            </Col>
            <Col sm={6}>
              <Form.Control type="number" placeholder="" />
            </Col>
          </Form.Group>
        </Form>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Komisi mengendap (hari)</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Minimal Komisi R</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Maximal Tarik Saldo</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Tujuan Notifikasi</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Awalan ID RS</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="text" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Defalt PIN</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Default Selisih</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Max No handphone</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Tiket Kadaluarsa (Menit)</Form.Label>
          </Col>
          <Col sm={6}>
            <Form.Control type="number" placeholder="" />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Col column sm={6}>
            <Form.Label className="position-relative">Cut Off System</Form.Label>
          </Col>
          <Col sm={6}>
            <DateRangePicker placeholder="Select Date Range" />
          </Col>
        </Form.Group>
      </fieldset>
    </>
  );
};

export default OptionsSystem;
