// import node module libraries
import React from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Button
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import FormatReqsData from "data/pengaturan/FormatReqsData";
import "rsuite/dist/rsuite-no-reset.min.css";

const FormatReqs = () => {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="h-100 border">
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Biaya</th>
            <th>Awalan</th>
            <th>Pemisah</th>
            <th>Param 1</th>
            <th>Param 2</th>
            <th>Param 3</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {FormatReqsData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.nama}</td>
                <td className="align-middle">{item.biaya}</td>
                <td className="align-middle">{item.awalan}</td>
                <td className="align-middle">{item.pemisah}</td>
                <td className="align-middle">{item.param1}</td>
                <td className="align-middle">{item.param2}</td>
                <td className="align-middle">{item.param3}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default FormatReqs;
