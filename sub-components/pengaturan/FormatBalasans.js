// import node module libraries
import React from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Button
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import FormatBalasansData from "data/pengaturan/FormatBalasansData";
import "rsuite/dist/rsuite-no-reset.min.css";

const FormatBalasans = () => {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="h-100 border">
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>ID</th>
            <th>Group</th>
            <th>Nama</th>
            <th>Format</th>
            <th>Action</th>
          </tr>
        </thead>
        <thead className="table-light">
          <tr>
            <th colSpan={5}>Group: Deposit</th>
          </tr>
        </thead>
        <tbody>
          {FormatBalasansData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.group}</td>
                <td className="align-middle">{item.nama}</td>
                <td className="align-middle">{item.format}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>

        <thead className="table-light">
          <tr>
            <th colSpan={5}>Group: Fasilitas Downline</th>
          </tr>
        </thead>
        <tbody>
          {FormatBalasansData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.group}</td>
                <td className="align-middle">{item.nama}</td>
                <td className="align-middle">{item.format}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>

        <thead className="table-light">
          <tr>
            <th colSpan={5}>Group: Payment Poin</th>
          </tr>
        </thead>
        <tbody>
          {FormatBalasansData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.group}</td>
                <td className="align-middle">{item.nama}</td>
                <td className="align-middle">{item.format}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default FormatBalasans;
