// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Modal,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import OperatorsData from "data/administrator/OperatorsData";
import "rsuite/dist/rsuite-no-reset.min.css";

const Operators = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="h-100 border">
      <Card.Header className="bg-white py-4">
        <Row>
          <Col lg={4} md={4} xs={4}>
            <Form>
              <Form.Group
                as={Row}
                className="mb-2"
                controlId="formHorizontalEmail"
              >
                <Col sm={12}>
                  <Button variant="primary" onClick={handleShow}>
                    Tambah Operator
                  </Button>
                  {/* Modal */}
                  <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                      <Modal.Title>Tambah Operator</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Form.Group
                          className="mb-2"
                          controlId="exampleForm.ControlInput1"
                        >
                          <Form.Label>Operator</Form.Label>
                          <Form.Control type="text" placeholder="" />
                        </Form.Group>
                        <Row className="mb-2">
                          <Form.Group as={Col} controlId="formGridMin">
                            <Form.Label>Min Digit</Form.Label>
                            <Form.Control type="number" />
                          </Form.Group>

                          <Form.Group as={Col} controlId="formGridMax">
                            <Form.Label>Max Digit</Form.Label>
                            <Form.Control type="number" />
                          </Form.Group>
                        </Row>
                        <Row className="mb-2">

                          <Form.Group as={Col} controlId="">
                            <Form.Label>Publish</Form.Label>
                            <Form.Check className="mb-2" label="Publish App" id="1" />
                            <Form.Check label="Publish Web" id="2" />
                          </Form.Group>

                          <Form.Group as={Col} controlId="formHorizontalCheck">
                            <Form.Label>Objek Pajak</Form.Label>
                            <Form.Check
                              className="mb-2"
                              type="radio"
                              label="PPH22"
                              name="formHorizontalRadios"
                              id="formHorizontalRadios1"
                            />
                            <Form.Check
                              type="radio"
                              label="PPH23"
                              name="formHorizontalRadios"
                              id="formHorizontalRadios2"
                            />
                          </Form.Group>
                        </Row>
                      </Form>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Batal
                      </Button>
                      <Button variant="primary" onClick={handleClose}>
                        Simpan
                      </Button>
                    </Modal.Footer>
                  </Modal>
                </Col>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </Card.Header>
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>Group</th>
            <th>Min Digit</th>
            <th>Max Digit</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {OperatorsData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.group}</td>
                <td className="align-middle">{item.mindigit}</td>
                <td className="align-middle">{item.maxdigit}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default Operators;
