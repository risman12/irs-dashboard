// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
} from "react-bootstrap";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import InventorysData from "data/administrator/InventorysData";
import "rsuite/dist/rsuite-no-reset.min.css";

const Inventorys = () => {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  return (
    <Card className="h-100 border">
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>ID</th>
            <th>Terminal</th>
            <th>Profil</th>
            <th>Denom</th>
            <th>Total</th>
            <th>Harga</th>
            <th>Stok</th>
            <th>Stok Aktual</th>
            <th>Update</th>
          </tr>
        </thead>
        <thead className="table-light">
          <tr>
            <th colSpan={9}>Terminal: Digipos</th>
          </tr>
        </thead>
        <tbody>
          {InventorysData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.terminal}</td>
                <td className="align-middle">{item.profil}</td>
                <td className="align-middle">{item.denom}</td>
                <td className="align-middle">{item.total}</td>
                <td className="align-middle">{item.harga}</td>
                <td className="align-middle">{item.stock}</td>
                <td className="align-middle">{item.stockactual}</td>
                <td className="align-middle">{item.update}</td>
              </tr>
            );
          })}
        </tbody>
        <thead className="table-light">
          <tr>
            <th colSpan={10}>Terminal: Sidompul</th>
          </tr>
        </thead>
        <tbody>
          {InventorysData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.terminal}</td>
                <td className="align-middle">{item.profil}</td>
                <td className="align-middle">{item.denom}</td>
                <td className="align-middle">{item.total}</td>
                <td className="align-middle">{item.harga}</td>
                <td className="align-middle">{item.stock}</td>
                <td className="align-middle">{item.stockactual}</td>
                <td className="align-middle">{item.update}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default Inventorys;
