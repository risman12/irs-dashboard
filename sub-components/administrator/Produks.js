// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Modal,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import ProduksData from "data/administrator/ProduksData";
import "rsuite/dist/rsuite-no-reset.min.css";

import Select from 'react-select';

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' }
]

const Produks = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <>
      <Row>
        <Col lg={4} md={4} xs={4}>
          <Form>
            <Form.Group
              as={Row}
              className="mb-2"
              controlId="formHorizontalEmail"
            >
              <Col sm={12}>
                <Button variant="primary" onClick={handleShow}>
                  Tambah
                </Button>
                {/* Modal */}
                <Modal show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Tambah Produk</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form>
                      <Form.Group
                        className="mb-2"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Produk</Form.Label>
                        <Select options={options} />
                      </Form.Group>
                      <Form.Group
                        className="mb-2"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Nama Produk</Form.Label>
                        <Form.Control type="text" placeholder="" />
                      </Form.Group>
                      <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Keterangan</Form.Label>
                        <Form.Control as="textarea" rows={3} />
                      </Form.Group>
                      <Row className="mb-2">
                        <Form.Group as={Col} controlId="formGridMin">
                          <Form.Label>Nominal</Form.Label>
                          <Form.Control type="number" />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridMax">
                          <Form.Label>Jenis</Form.Label>
                          <Select options={options} />
                        </Form.Group>
                      </Row>
                      <Row className="mb-2">
                        <Form.Group as={Col} controlId="formGridMin">
                          <Form.Label>Kode</Form.Label>
                          <Form.Control type="number" />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridMax">
                          <Form.Label>Tanpa Kode</Form.Label>
                          <Form.Control type="text" />
                        </Form.Group>
                      </Row>
                    </Form>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Batal
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                      Simpan
                    </Button>
                  </Modal.Footer>
                </Modal>
              </Col>
            </Form.Group>
          </Form>
        </Col>
      </Row>

      <Table responsive hover className="text-nowrap mb-0 mt-2">
        <thead className="table-light">
          <tr>
            <th>Operator</th>
            <th>Nama Produk</th>
            <th>Kode Produk</th>
            <th>Nominal</th>
            <th>Jenis Teks</th>
            <th>Action</th>
          </tr>
        </thead>
        <thead className="table-light">
          <tr>
            <th colSpan={6}>Operator: Indosat</th>
          </tr>
        </thead>
        <tbody>
          {ProduksData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.operator}</td>
                <td className="align-middle">{item.produk}</td>
                <td className="align-middle">{item.kodeproduk}</td>
                <td className="align-middle">{item.nominal}</td>
                <td className="align-middle">{item.jenistext}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>

        <thead className="table-light">
          <tr>
            <th colSpan={6}>Operator: Axis</th>
          </tr>
        </thead>
        <tbody>
          {ProduksData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.operator}</td>
                <td className="align-middle">{item.produk}</td>
                <td className="align-middle">{item.kodeproduk}</td>
                <td className="align-middle">{item.nominal}</td>
                <td className="align-middle">{item.jenistext}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
};

export default Produks;
