// import node module libraries
import React from "react";
import { Card, Table } from "react-bootstrap";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import AreasData from "data/administrator/AreasData";
import "rsuite/dist/rsuite-no-reset.min.css";

const Areas = () => {
  return (
    <Card className="h-100 border">
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>Nama Area</th>
          </tr>
        </thead>
        <tbody>
          {AreasData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.namaarea}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default Areas;
