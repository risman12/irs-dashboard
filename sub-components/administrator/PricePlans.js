// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Modal,
  ListGroup
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import PricePlansData from "data/administrator/PricePlansData";
import "rsuite/dist/rsuite-no-reset.min.css";

const PricePlans = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <>
      <Row>
        <Col xl={4} lg={4} md={4} xs={4}>
          <ListGroup defaultActiveKey="#link1" className="list-terminal">
            <ListGroup.Item action href="#link1">
              Center SMS
            </ListGroup.Item>
            <ListGroup.Item action href="#link2">
              Sender SMS
            </ListGroup.Item>
            <ListGroup.Item action href="#link3">
              Center SMS & Sender SMS
            </ListGroup.Item>
            <ListGroup.Item action href="#link4">
              QPAY
            </ListGroup.Item>
            <ListGroup.Item action href="#link5">
              IRS Market
            </ListGroup.Item>
            <ListGroup.Item action href="#link6">
              ISIMPEL
            </ListGroup.Item>
            <ListGroup.Item action href="#link7">
              SIDOMPUL
            </ListGroup.Item>
            <ListGroup.Item action href="#link8">
              DIGIPOS
            </ListGroup.Item>
            <ListGroup.Item action href="#link9">
              Telegram
            </ListGroup.Item>
            <ListGroup.Item action href="#link10">
              Center Jabber
            </ListGroup.Item>
            <ListGroup.Item action href="#link11">
              Center Gmail
            </ListGroup.Item>
            <ListGroup.Item action href="#link12">
              Center Whatsapp
            </ListGroup.Item>
          </ListGroup>
        </Col>
        <Col xl={8} lg={8} md={8} xs={8}>
          <Row>
            <Col lg={4} md={4} xs={4}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Control type="number" placeholder="Max Selisih" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Control type="number" placeholder="Max Ticket" />
                  </Col>
                </Form.Group>
              </Form>
            </Col>
            <Col lg={8} md={8} xs={8}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-1 pt-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={6}>
                    <Form.Check
                      type="checkbox"
                      id="autoSizingCheck[1]"
                      label="SMS Promo"
                    />
                  </Col>
                  <Col sm={6}>
                    <Form.Check
                      type="checkbox"
                      id="autoSizingCheck[2]"
                      label="Ticket Deposit"
                    />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-1 pt-3"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={6}>
                    <Form.Check
                      type="checkbox"
                      id="autoSizingCheck[3]"
                      label="Poin"
                    />
                  </Col>
                  <Col sm={6}>
                    <Form.Check
                      type="checkbox"
                      id="autoSizingCheck[4]"
                      label="MLM Fix"
                    />
                  </Col>
                </Form.Group>
              </Form>
            </Col>
          </Row>
          <Table responsive hover className="text-nowrap">
            <thead className="table-light">
              <tr>
                <th>Group</th>
                <th>Min Digit</th>
                <th>Max Digit</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {PricePlansData.map((item, index) => {
                return (
                  <tr key={index}>
                    <td className="align-middle">{item.group}</td>
                    <td className="align-middle">{item.mindigit}</td>
                    <td className="align-middle">{item.maxdigit}</td>
                    <td className="align-middle">
                      <ActionMenu />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </>
  );
};

export default PricePlans;
