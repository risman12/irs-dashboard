// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Modal,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import TerminalsData from "data/administrator/TerminalsData";
import "rsuite/dist/rsuite-no-reset.min.css";

const Terminals = () => {
  const [modalState, setModalState] = useState("close");

  const handleShowModalOne = () => {
    setModalState("modal-one")
  }

  const handleShowModalTwo = () => {
    setModalState("modal-two")
  }

  const handleShowModalThree = () => {
    setModalState("modal-three")
  }

  const handleShowModalFour = () => {
    setModalState("modal-four")
  }

  const handleClose = () => {
    setModalState("close")
  }

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="h-100 border">
      <Card.Header className="bg-white py-4">
        <Row>
          <Col lg={12} md={12} xs={12}>
            <Form>
              <Form.Group
                as={Row}
                className="mb-2"
                controlId="formHorizontalEmail"
              >
                <Col sm={12}>
                  <Button variant="light" onClick={handleShowModalOne}>
                    <i className="fe fe-user me-2"></i>
                    Supplier
                  </Button>
                  {/* Modal Supplier */}
                  <Modal show={modalState === "modal-one"}>
                    <Modal.Header>
                      <Modal.Title>Tambah Supplier</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Row className="align-items-center">
                          <Form>
                            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
                              <Form.Control type="text" placeholder="Nama Supplier" />
                            </Form.Group>
                            <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                              <Form.Control as="textarea" rows={2} placeholder="Alamat" />
                            </Form.Group>
                          </Form>
                          <Col xs="auto">
                            <Button variant="primary" className="mb-2">
                              Simpan
                            </Button>
                          </Col>
                        </Row>
                      </Form>
                      <Table responsive hover className="text-nowrap mt-2 mb-0">
                        <thead className="table-light">
                          <tr>
                            <th>ID</th>
                            <th>Supplier</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {TerminalsData.map((item, index) => {
                            return (
                              <tr key={index}>
                                <td className="align-middle">{item.id}</td>
                                <td className="align-middle">{item.supplier}</td>
                                <td className="align-middle">
                                  <ActionMenu />
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </Table>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Tutup
                      </Button>
                    </Modal.Footer>
                  </Modal>

                  <Button variant="light" className="ms-3" onClick={handleShowModalTwo}>
                    <i className="fe fe-user me-2"></i>
                    Profil
                  </Button>
                  {/* Modal Profile */}
                  <Modal show={modalState === "modal-two"}>
                    <Modal.Header>
                      <Modal.Title>Tambah Profil</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Row className="mb-2">
                          <Form.Group as={Col} controlId="formGridEmail">
                            <Form.Control type="text" placeholder="Nama Profile" />
                          </Form.Group>

                          <Form.Group as={Col} controlId="formGridPassword">
                            <Form.Control type="text" placeholder="Denom" />
                          </Form.Group>
                        </Row>
                        <Button variant="primary" type="submit">
                          Simpan
                        </Button>
                      </Form>
                      <Table responsive hover className="text-nowrap mt-2 mb-0">
                        <thead className="table-light">
                          <tr>
                            <th>ID</th>
                            <th>Nama Profile</th>
                            <th>Denom</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                      </Table>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Tutup
                      </Button>
                    </Modal.Footer>
                  </Modal>

                  <Button variant="light" className="ms-3" onClick={handleShowModalThree}>
                    <i className="fe fe-user me-2"></i>
                    User Jabber
                  </Button>
                  {/* Modal Jabber */}
                  <Modal show={modalState === "modal-three"}>
                    <Modal.Header>
                      <Modal.Title>User Jabber Operation</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Row className="mb-2">
                          <Form.Group as={Col} controlId="formGridEmail">
                            <Form.Control type="text" placeholder="Jenis" />
                          </Form.Group>

                          <Form.Group as={Col} controlId="formGridPassword">
                            <Form.Control type="text" placeholder="User" />
                          </Form.Group>
                        </Row>
                        <Button variant="primary" type="submit">
                          Simpan
                        </Button>
                      </Form>
                      <Table responsive hover className="text-nowrap mt-2 mb-0">
                        <thead className="table-light">
                          <tr>
                            <th>ID</th>
                            <th>Jenis</th>
                            <th>User</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                      </Table>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Tutup
                      </Button>
                    </Modal.Footer>
                  </Modal>

                  <Button variant="light" className="ms-3" onClick={handleShowModalFour}>
                    <i className="fe fe-book me-2"></i>
                    Auto SN
                  </Button>
                  {/* Modal SN */}
                  <Modal show={modalState === "modal-four"}>
                    <Modal.Header>
                      <Modal.Title>Setting Auto SN</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Row className="align-items-center">
                          <Col xs="6">
                            <Form.Control
                              className="mb-2"
                              id="inlineFormInput"
                              placeholder="Kata"
                            />
                          </Col>
                          <Col xs="auto">
                            <Button variant="primary" className="mb-2">
                              Simpan
                            </Button>
                          </Col>
                        </Row>
                      </Form>
                      <Table responsive hover className="text-nowrap mt-2 mb-0">
                        <thead className="table-light">
                          <tr>
                            <th>ID</th>
                            <th>Kata</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                      </Table>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Tutup
                      </Button>
                    </Modal.Footer>
                  </Modal>
                </Col>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </Card.Header>
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>ID</th>
            <th>Terminal</th>
            <th>Sebagai</th>
            <th>Rangkap</th>
            <th>Supplier</th>
            <th>Com</th>
            <th>Baud Rate</th>
            <th>HPStr</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {TerminalsData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.terminal}</td>
                <td className="align-middle">{item.sebagai}</td>
                <td className="align-middle">{item.rangkap}</td>
                <td className="align-middle">{item.supplier}</td>
                <td className="align-middle">{item.com}</td>
                <td className="align-middle">{item.baudrate}</td>
                <td className="align-middle">{item.hpstr}</td>
                <td className="align-middle">{item.status}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default Terminals;
