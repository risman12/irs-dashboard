// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Alert,
  Tabs,
  Tab
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import HistoryResellersData from "data/administrator/HistoryResellersData";
import "rsuite/dist/rsuite-no-reset.min.css";

import { DateRangePicker } from "rsuite";
import Select from "react-select";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const HistoryResellers = () => {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <>
      <Card>
        <Card.Body className="border rounded">
          <Row>
            <Col lg={3} md={3} xs={3}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Select options={options} />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Control as="textarea" rows={2} placeholder="Alamat" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <DateRangePicker placeholder="Select Date Range" />
                  </Col>
                </Form.Group>
              </Form>
            </Col>
            <Col lg={5} md={5} xs={5}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Control type="text" placeholder="Upline" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Control as="textarea" rows={2} placeholder="" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={6}>
                    <Button variant="primary">Export</Button>
                  </Col>
                </Form.Group>
              </Form>
            </Col>
            <Col lg={4} md={4} xs={4}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Form.Label column sm={3}>
                    Saldo
                  </Form.Label>
                  <Col sm={9}>
                    <Alert variant="success" className="mb-0">
                      <p className="mb-0 text-end">2400</p>
                    </Alert>
                  </Col>
                </Form.Group>
              </Form>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Form.Label column sm={3}>
                    Poin
                  </Form.Label>
                  <Col sm={9}>
                    <Alert variant="warning" className="mb-0">
                      <p className="mb-0 text-end">10</p>
                    </Alert>
                  </Col>
                </Form.Group>
              </Form>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Form.Label column sm={3}>
                    Komisi
                  </Form.Label>
                  <Col sm={9}>
                    <Alert variant="danger" className="mb-0">
                      <p className="mb-0 text-end">120</p>
                    </Alert>
                  </Col>
                </Form.Group>
              </Form>
            </Col>
          </Row>
        </Card.Body>
      </Card>
      <Tabs
        defaultActiveKey="historysaldo"
        id="noanim-tab-example"
        className="tabs-custome mt-3"
      >
        <Tab eventKey="historysaldo" title="History Saldo" className="border border-top-0 rounded-bottom p-4">
          <Table responsive hover className="text-nowrap mb-0">
            <thead className="table-light">
              <tr>
                <th>ID</th>
                <th>Tanggal</th>
                <th>Jam</th>
                <th>Keterangan</th>
                <th>Mutasi</th>
                <th>Saldo</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {HistoryResellersData.map((item, index) => {
                return (
                  <tr key={index}>
                    <td className="align-middle">{item.id}</td>
                    <td className="align-middle">{item.tgl}</td>
                    <td className="align-middle">{item.jam}</td>
                    <td className="align-middle">{item.ket}</td>
                    <td className="align-middle">{item.mutasi}</td>
                    <td className="align-middle">{item.saldo}</td>
                    <td className="align-middle">
                      <ActionMenu />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Tab>
        <Tab eventKey="transaksi" title="Transaksi" className="border border-top-0 rounded-bottom p-4">
          Tab content for Transaksi
        </Tab>
        <Tab eventKey="stokunit" title="Stok Unit" className="border border-top-0 rounded-bottom p-4">
          Tab content for Contact
        </Tab>
        <Tab eventKey="fasilitas" title="Fasilitas" className="border border-top-0 rounded-bottom p-4">
          Tab content for Contact
        </Tab>
        <Tab eventKey="topupsaldo" title="Topup Saldo" className="border border-top-0 rounded-bottom p-4">
          Tab content for Contact
        </Tab>
        <Tab eventKey="transfersaldo" title="Transfer Saldo" className="border border-top-0 rounded-bottom p-4">
          Tab content for Contact
        </Tab>
        <Tab eventKey="downline" title="Downline" className="border border-top-0 rounded-bottom p-4">
          Tab content for Contact
        </Tab>
        <Tab eventKey="komisi" title="Komisi" className="border border-top-0 rounded-bottom p-4">
          Tab content for Contact
        </Tab>
      </Tabs>
    </>
  );
};

export default HistoryResellers;
