// import node module libraries
import React from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  InputGroup,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import LogUsersData from "data/administrator/LogUsersData";
import "rsuite/dist/rsuite-no-reset.min.css";

import { DateRangePicker } from "rsuite";

const LogUsers = () => {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="h-100 border">
      <Card.Header className="bg-white py-4">
        <Row>
          <Col lg={12} md={12} xs={12}>
            <Form>
              <Row className="align-items-center">
                <Col xs="auto">
                  <DateRangePicker placeholder="Select Date Range" />
                </Col>
                <Col xs="auto">
                  <Form.Control id="inlineFormInputGroup" placeholder="Cari" />
                </Col>
                <Col xs="auto">
                  <Button variant="primary">Cari</Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Card.Header>
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>ID</th>
            <th>Waktu</th>
            <th>User</th>
            <th>Keterangan</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {LogUsersData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.waktu}</td>
                <td className="align-middle">{item.user}</td>
                <td className="align-middle">{item.ket}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default LogUsers;
