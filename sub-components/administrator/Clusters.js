// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Modal,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import ClustersData from "data/administrator/ClustersData";
import "rsuite/dist/rsuite-no-reset.min.css";

const Clusters = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="h-100 border">
      <Card.Header className="bg-white py-4">
        <Row>
          <Col lg={3} md={3} xs={3}>
            <Form>
              <Form.Group
                as={Row}
                className="mb-2"
                controlId="formHorizontalEmail"
              >
                <Col sm={12}>
                  <Button variant="primary" onClick={handleShow}>
                    Tambah
                  </Button>
                  {/* Modal */}
                  <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                      <Modal.Title>Tambah Cluster</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Form.Group
                          className="mb-2"
                          controlId="exampleForm.ControlTextarea1"
                        >
                          <Form.Label>Cluster</Form.Label>
                          <Form.Control type="text" placeholder="" />
                        </Form.Group>
                      </Form>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Batal
                      </Button>
                      <Button variant="primary" onClick={handleClose}>
                        Simpan
                      </Button>
                    </Modal.Footer>
                  </Modal>
                </Col>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </Card.Header>
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>ID</th>
            <th>Pesan</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {ClustersData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.cluster}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default Clusters;
