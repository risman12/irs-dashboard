// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Modal,
  Tabs,
  Tab
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import UsersData from "data/administrator/UsersData";
import "rsuite/dist/rsuite-no-reset.min.css";

import Select from "react-select";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const Users = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <>
      <Tabs
        defaultActiveKey="user"
        transition={false}
        id="noanim-tab-example"
        className="tabs-custome"
      >
        <Tab eventKey="user" title="User" className="border border-top-0 rounded-bottom p-4">
          <Form>
            <Form.Group
              as={Row}
              className="mb-2"
              controlId="formHorizontalEmail"
            >
              <Col sm={12}>
                <Button variant="primary" onClick={handleShow}>
                  Tambah
                </Button>
                {/* Modal */}
                <Modal show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Tambah User</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form>
                      <Form.Group
                        className="mb-2"
                        controlId="exampleForm.ControlTextarea1"
                      >
                        <Form.Label>User</Form.Label>
                        <Form.Control type="text" placeholder="" />
                      </Form.Group>
                      <Form.Group
                        className="mb-2"
                        controlId="exampleForm.ControlTextarea1"
                      >
                        <Form.Label>Nama</Form.Label>
                        <Form.Control type="text" placeholder="" />
                      </Form.Group>
                      <Form.Group
                        className="mb-2"
                        controlId="exampleForm.ControlTextarea1"
                      >
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="" />
                      </Form.Group>
                      <Form.Group
                        className="mb-2"
                        controlId="exampleForm.ControlTextarea1"
                      >
                        <Form.Label>Hak Akses</Form.Label>
                        <Select options={options} />
                      </Form.Group>
                      <Row className="mb-2">
                        <Form.Label>Waktu Login</Form.Label>
                        <Form.Group as={Col} controlId="formGridEmail">
                          <Form.Control type="email" placeholder="" />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridPassword">
                          <Form.Control type="password" placeholder="" />
                        </Form.Group>
                      </Row>
                      <Form.Group
                        className="mb-2"
                        controlId="exampleForm.ControlTextarea1"
                      >
                        <Form.Check
                          type="checkbox"
                          id="autoSizingCheck"
                          label="Auto Refresh"
                        />
                      </Form.Group>
                    </Form>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Batal
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                      Simpan
                    </Button>
                  </Modal.Footer>
                </Modal>
              </Col>
            </Form.Group>
          </Form>

          <Table responsive hover className="text-nowrap mb-0 mt-2">
            <thead className="table-light">
              <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Nama Lengkap</th>
                <th>Akses</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {UsersData.map((item, index) => {
                return (
                  <tr key={index}>
                    <td className="align-middle">{item.id}</td>
                    <td className="align-middle">{item.nama}</td>
                    <td className="align-middle">{item.namalengkap}</td>
                    <td className="align-middle">{item.akses}</td>
                    <td className="align-middle">
                      <ActionMenu />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Tab>
        <Tab eventKey="hakakses" title="Hak Akses" className="border border-top-0 rounded-bottom p-4">
          <Row>
            <Col xl={6} lg={6} md={6} xs={6}>
              <Button variant="primary">
                Tambah
              </Button>
              <Table responsive hover className="text-nowrap mb-0 mt-2">
                <thead className="table-light">
                  <tr>
                    <th>ID</th>
                    <th>Hak Akses</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {UsersData.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td className="align-middle">{item.id}</td>
                        <td className="align-middle">{item.akses}</td>
                        <td className="align-middle">
                          <ActionMenu />
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </Col>
            <Col xl={6} lg={6} md={6} xs={6}>
              <Row>
                <Col md={6}>
                  <fieldset class="border rounded-3 py-2 px-3 h-100">
                    <legend class="float-none w-auto px-3 mb-0" >Transaksi</legend>
                    <Form.Check
                      type="checkbox"
                      id="transaksi[0]"
                      label="Trx Group Menu"
                    />
                    <Form.Check
                      type="checkbox"
                      id="transaksi[1]"
                      label="Penjualan Menu Akses"
                    />
                    <Form.Check
                      type="checkbox"
                      id="transaksi[2]"
                      label="Sukseskan Trx"
                    />
                    <Form.Check
                      type="checkbox"
                      id="transaksi[3]"
                      label="Gagalkan Trx"
                    />
                    <Form.Check
                      type="checkbox"
                      id="transaksi[4]"
                      label="Kirim Ulang Trx"
                    />
                    <Form.Check
                      type="checkbox"
                      id="transaksi[5]"
                      label="Set Produk Aktif"
                    />
                    <Form.Check
                      type="checkbox"
                      id="transaksi[6]"
                      label="Sender Outbox"
                    />
                  </fieldset>
                </Col>
                <Col md={6}>
                  <fieldset class="border rounded-3 py-2 px-3 h-100">
                    <legend class="float-none w-auto px-3 mb-0" >Administrator</legend>
                    <Form.Check
                      type="checkbox"
                      id="administrator[0]"
                      label="Administrator Group Menu"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[1]"
                      label="Group Operator"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[2]"
                      label="Produk"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[3]"
                      label="Terminal"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[4]"
                      label="History Reseller"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[5]"
                      label="Data Reseller"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[6]"
                      label="Ganti Upline"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[7]"
                      label="User Menu Akses"
                    />
                    <Form.Check
                      type="checkbox"
                      id="administrator[8]"
                      label="Log User Akses"
                    />
                  </fieldset>
                </Col>
              </Row>
              <Row className="mt-3">
                <Col md={6}>
                  <fieldset class="border rounded-3 py-2 px-3 h-100">
                    <legend class="float-none w-auto px-3 mb-0" >Keuangan</legend>
                    <Form.Check
                      type="checkbox"
                      id="keuangan[0]"
                      label="Finance Group Menu"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[1]"
                      label="Laporan Transaksi"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[2]"
                      label="Komisi & Poin"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[3]"
                      label="Tiket Deposit"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[4]"
                      label="Daftar Akun"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[4]"
                      label="Neraca"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[4]"
                      label="Hutang"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[5]"
                      label="Piutang"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[6]"
                      label="Pembelian"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[7]"
                      label="Deposit"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[8]"
                      label="Input Piutang / Bon Saldo"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[9]"
                      label="Topup Saldo"
                    />
                    <Form.Check
                      type="checkbox"
                      id="keuangan[10]"
                      label="Jurnal"
                    />
                  </fieldset>
                </Col>
                <Col md={6}>
                  <fieldset class="border rounded-3 py-2 px-3 h-100">
                    <legend class="float-none w-auto px-3 mb-0" >Pengaturan</legend>
                    <Form.Check
                      type="checkbox"
                      id="pengaturan[0]"
                      label="Pengaturan Group Menu"
                    />
                    <Form.Check
                      type="checkbox"
                      id="pengaturan[1]"
                      label="Format Request"
                    />
                    <Form.Check
                      type="checkbox"
                      id="pengaturan[2]"
                      label="Format Balasan"
                    />
                    <Form.Check
                      type="checkbox"
                      id="pengaturan[3]"
                      label="Utility"
                    />
                    <Form.Check
                      type="checkbox"
                      id="pengaturan[4]"
                      label="Kirim Ulang Trx"
                    />
                  </fieldset>
                </Col>
              </Row>
            </Col>
          </Row>
        </Tab>
      </Tabs>
    </>
  );
};

export default Users;
