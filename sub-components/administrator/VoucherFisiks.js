// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import VoucherFisiksData from "data/administrator/VoucherFisiksData";
import "rsuite/dist/rsuite-no-reset.min.css";
import Select from "react-select";

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const VoucherFisiks = () => {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <>
      <Row>
        <Col lg={12} md={12} xs={12}>
          <Form>
            <Row className="align-items-center">
              <Col xs="4">
                <Select options={options} />
              </Col>
              <Col xs="2">
                <Form.Control id="inlineFormInputGroup" placeholder="Cari" />
              </Col>
              <Col xs="2">
                <Button variant="primary">Cari</Button>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>

      <Table responsive hover className="text-nowrap mb-0 mt-2">
        <thead className="table-light">
          <tr>
            <th>ID</th>
            <th>Tanggal</th>
            <th>Jam</th>
            <th>Kode Produk</th>
            <th>Nama Produk</th>
            <th>Masa Aktif</th>
            <th>No Voucher</th>
            <th>No Seri</th>
            <th>Status</th>
            <th>ID Trx</th>
            <th>Gagal</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {VoucherFisiksData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.id}</td>
                <td className="align-middle">{item.tanggal}</td>
                <td className="align-middle">{item.jam}</td>
                <td className="align-middle">{item.kode}</td>
                <td className="align-middle">{item.namaproduk}</td>
                <td className="align-middle">{item.masaaktif}</td>
                <td className="align-middle">{item.novoucher}</td>
                <td className="align-middle">{item.noseri}</td>
                <td className="align-middle">{item.status}</td>
                <td className="align-middle">{item.idtrx}</td>
                <td className="align-middle">{item.gagal}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
};

export default VoucherFisiks;
