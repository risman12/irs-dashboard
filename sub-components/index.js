/**
 * The folder sub-components contains sub component of all the pages,
 * so here you will find folder names which are listed in root pages.
 */

// sub components for /pages/dashboard
import ActiveProjects from "sub-components/dashboard/ActiveProjects";
import Transaksis from "sub-components/dashboard/Transaksis";
import Senders from "sub-components/dashboard/Senders";
import Komplains from "sub-components/dashboard/Komplains";
import PesanTambahans from "./dashboard/PesanTambahans";
import TraceInboxs from "./dashboard/TraceInboxs";
import LogTerminals from "./dashboard/LogTerminals";
import LogMonitorings from "./dashboard/LogMonitorings";
import Operators from "./administrator/Operators";
import Areas from "./administrator/Areas";
import Produks from "./administrator/Produks";
import Terminals from "./administrator/Terminals";
import VoucherFisiks from "./administrator/VoucherFisiks";
import PricePlans from "./administrator/PricePlans";
import Clusters from "./administrator/Clusters";
import Blacklists from "./administrator/Blacklists";
import HistoryResellers from "./administrator/HistoryResellers";
import Users from "./administrator/Users";
import LogUsers from "./administrator/LogUsers";
import Inventorys from "./administrator/Inventorys";
import FormatReqs from "./pengaturan/FormatReqs";
import FormatBalasans from "./pengaturan/FormatBalasans";
import Utilitys from "./pengaturan/Utilitys";
import UtilitysBtn from "./pengaturan/UtilitysBtn";
import OptionsSystem from "./pengaturan/OptionsSystem";
import OptionsBisnis from "./pengaturan/OptionsBisnis";
import LaporanPenjualans from "./laporan/LaporanPenjualans";

export {
    ActiveProjects,
    Transaksis,
    Senders,
    Komplains,
    PesanTambahans,
    TraceInboxs,
    LogTerminals,
    LogMonitorings,
    Operators,
    Areas,
    Produks,
    Terminals,
    VoucherFisiks,
    PricePlans,
    Clusters,
    Blacklists,
    HistoryResellers,
    Users,
    LogUsers,
    FormatReqs,
    Inventorys,
    FormatBalasans,
    Utilitys,
    UtilitysBtn,
    OptionsSystem,
    OptionsBisnis,
    LaporanPenjualans,
};
