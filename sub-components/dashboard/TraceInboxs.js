// import node module libraries
import React from "react";
import Link from "next/link";
import { Card, Table, Dropdown, Row, Col, Form, Button } from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import TraceInboxsData from "data/dashboard/TraceInboxsData";
import "rsuite/dist/rsuite-no-reset.min.css";

import { DateRangePicker } from "rsuite";

const TraceInboxs = () => {
    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <Link
            href=""
            ref={ref}
            onClick={(e) => {
                e.preventDefault();
                onClick(e);
            }}
            className="text-muted text-primary-hover"
        >
            {children}
        </Link>
    ));

    CustomToggle.displayName = "CustomToggle";

    const ActionMenu = () => {
        return (
            <Dropdown>
                <Dropdown.Toggle as={CustomToggle}>
                    <MoreVertical size="15px" className="text-muted" />
                </Dropdown.Toggle>
                <Dropdown.Menu className="action" align={"end"}>
                    <Dropdown.Item eventKey="1">
                        <div class="d-grid gap-2">
                            <Button variant="warning" className="d-block">
                                Edit
                            </Button>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="2">
                        <div class="d-grid gap-2">
                            <Button variant="danger">Delete</Button>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="3">
                        <div class="d-grid gap-2">
                            <Button variant="info">Detail</Button>
                        </div>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        );
    };

    return (
        <>
            <Card className="border">
                <Card.Header className="bg-white py-4">
                    <Row>
                        <Col lg={3} md={3} xs={3}>
                            <Form>
                                <Form.Group
                                    as={Row}
                                    className="mb-2"
                                    controlId="formHorizontalEmail"
                                >
                                    <Col sm={12}>
                                        <DateRangePicker placeholder="Select Date Range" />
                                    </Col>
                                </Form.Group>
                                <Form.Group
                                    as={Row}
                                    className="mb-2"
                                    controlId="formHorizontalEmail"
                                >
                                    <Col sm={12}>
                                        <Form.Group
                                            as={Row}
                                            className="mb-2"
                                            controlId="formHorizontalEmail"
                                        >
                                            <Col sm={12}>
                                                <Form.Control type="text" placeholder="Isi Pesan" />
                                            </Col>
                                        </Form.Group>
                                    </Col>
                                </Form.Group>
                            </Form>
                        </Col>
                        <Col lg={3} md={3} xs={3}>
                            <Form>
                                <Form.Group
                                    as={Row}
                                    className="mb-2"
                                    controlId="formHorizontalEmail"
                                >
                                    <Col sm={12}>
                                        <Form.Control type="text" placeholder="Nama" />
                                    </Col>
                                </Form.Group>
                                <Form.Group
                                    as={Row}
                                    className="mb-2"
                                    controlId="formHorizontalEmail"
                                >
                                    <Col sm={12}>
                                        <Form.Control type="text" placeholder="Pengirim" />
                                    </Col>
                                </Form.Group>
                            </Form>
                        </Col>
                        <Col lg={3} md={3} xs={3}>
                            <Button variant="primary">Cari</Button>
                        </Col>
                    </Row>
                </Card.Header>
                <Table responsive hover className="text-nowrap">
                    <thead className="table-light">
                        <tr>
                            <th>ID Transaksi</th>
                            <th>Waktu</th>
                            <th>ID Reseller</th>
                            <th>Terminal</th>
                            <th>Pengirim</th>
                            <th>Isi Komplain</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {TraceInboxsData.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td className="align-middle">{item.idtrx}</td>
                                    <td className="align-middle">{item.waktu}</td>
                                    <td className="align-middle">{item.idrs}</td>
                                    <td className="align-middle">{item.terminal}</td>
                                    <td className="align-middle">{item.pengirim}</td>
                                    <td className="align-middle">{item.isikomplain}</td>
                                    <td className="align-middle">{item.status}</td>
                                    <td className="align-middle">
                                        <ActionMenu />
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </Card>

            <Card className="border mt-3">
                <Table responsive hover className="text-nowrap">
                    <thead className="table-light">
                        <tr>
                            <th>Waktu</th>
                            <th>Nomor</th>
                            <th>Status</th>
                            <th>Isi</th>
                            <th>ID Transaksi</th>
                            <th>Lokasi</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {TraceInboxsData.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td className="align-middle">{item.waktu}</td>
                                    <td className="align-middle">{item.idrs}</td>
                                    <td className="align-middle">{item.status}</td>
                                    <td className="align-middle">{item.isikomplain}</td>
                                    <td className="align-middle">{item.idtrx}</td>
                                    <td className="align-middle">{item.status}</td>
                                    <td className="align-middle">
                                        <ActionMenu />
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </Card>
        </>
    );
};

export default TraceInboxs;
