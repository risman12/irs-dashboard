// import node module libraries
import React from "react";
import Link from "next/link";
import {
    Card,
    Table,
    Dropdown,
    Row,
    Col,
    Form,
    Alert,
    Button,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import TransaksisData from "data/dashboard/TransaksisData";
import "rsuite/dist/rsuite-no-reset.min.css";

import { DateRangePicker } from "rsuite";
import Select from "react-select";

const options = [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" },
];

const Transaksis = () => {
    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <Link
            href=""
            ref={ref}
            onClick={(e) => {
                e.preventDefault();
                onClick(e);
            }}
            className="text-muted text-primary-hover"
        >
            {children}
        </Link>
    ));

    CustomToggle.displayName = "CustomToggle";

    const ActionMenu = () => {
        return (
            <Dropdown>
                <Dropdown.Toggle as={CustomToggle}>
                    <MoreVertical size="15px" className="text-muted" />
                </Dropdown.Toggle>
                <Dropdown.Menu className="action" align={"end"}>
                    <Dropdown.Item eventKey="1">
                        <div class="d-grid gap-2">
                            <Button variant="warning" className="d-block">
                                Edit
                            </Button>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="2">
                        <div class="d-grid gap-2">
                            <Button variant="danger">Delete</Button>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="3">
                        <div class="d-grid gap-2">
                            <Button variant="info">Detail</Button>
                        </div>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        );
    };

    return (
        <Card className="h-100 border">
            <Card.Header className="bg-white py-4">
                <Row>
                    <Col lg={3} md={3} xs={3}>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <DateRangePicker placeholder="Select Date Range" />
                                </Col>
                            </Form.Group>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <Select options={options} />
                                </Col>
                            </Form.Group>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <Select options={options} />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col lg={5} md={5} xs={5}>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <Form.Control type="text" placeholder="Nama Reseller" />
                                </Col>
                            </Form.Group>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={6}>
                                    <Form.Control type="text" placeholder="Tujuan" />
                                </Col>
                                <Col sm={6}>
                                    <Form.Control type="text" placeholder="Produk" />
                                </Col>
                            </Form.Group>
                            <Form.Group
                                as={Row}
                                className="mb-1 pt-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={6}>
                                    <Form.Check
                                        type="checkbox"
                                        id="autoSizingCheck[1]"
                                        label="History"
                                    />
                                </Col>
                                <Col sm={6}>
                                    <Form.Check
                                        type="checkbox"
                                        id="autoSizingCheck[2]"
                                        label="Auto Refresh"
                                    />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col lg={4} md={4} xs={4}>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Form.Label column sm={3}>
                                    Sukses
                                </Form.Label>
                                <Col sm={9}>
                                    <Alert variant="success" className="mb-0">
                                        <p className="mb-0 text-end">2400</p>
                                    </Alert>
                                </Col>
                            </Form.Group>
                        </Form>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Form.Label column sm={3}>
                                    Pending
                                </Form.Label>
                                <Col sm={9}>
                                    <Alert variant="warning" className="mb-0">
                                        <p className="mb-0 text-end">10</p>
                                    </Alert>
                                </Col>
                            </Form.Group>
                        </Form>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Form.Label column sm={3}>
                                    Gagal
                                </Form.Label>
                                <Col sm={9}>
                                    <Alert variant="danger" className="mb-0">
                                        <p className="mb-0 text-end">120</p>
                                    </Alert>
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
            </Card.Header>
            <Table responsive hover className="text-nowrap">
                <thead className="table-light ">
                    <tr>
                        <th>Id Transaksi</th>
                        <th>Reff Client</th>
                        <th>Id</th>
                        <th>Nama</th>
                        <th>Tujuan</th>
                        <th>Sn</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {TransaksisData.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td className="align-middle">{item.idtrx}</td>
                                <td className="align-middle">{item.reffclient}</td>
                                <td className="align-middle">{item.id}</td>
                                <td className="align-middle">{item.nama}</td>
                                <td className="align-middle">{item.tujuan}</td>
                                <td className="align-middle">{item.sn}</td>
                                <td className="align-middle">{item.status}</td>
                                <td className="align-middle">
                                    <ActionMenu />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </Card>
    );
};

export default Transaksis;
