// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
    Card,
    Table,
    Dropdown,
    Row,
    Col,
    Form,
    Button,
    Modal,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import KomplainsData from "data/dashboard/KomplainsData";
import "rsuite/dist/rsuite-no-reset.min.css";

import { DateRangePicker } from "rsuite";
import Select from 'react-select';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
]

const Senders = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <Link
            href=""
            ref={ref}
            onClick={(e) => {
                e.preventDefault();
                onClick(e);
            }}
            className="text-muted text-primary-hover"
        >
            {children}
        </Link>
    ));

    CustomToggle.displayName = "CustomToggle";

    const ActionMenu = () => {
        return (
            <Dropdown>
                <Dropdown.Toggle as={CustomToggle}>
                    <MoreVertical size="15px" className="text-muted" />
                </Dropdown.Toggle>
                <Dropdown.Menu className="action" align={"end"}>
                    <Dropdown.Item eventKey="1">
                        <div class="d-grid gap-2">
                            <Button variant="warning" className="d-block">
                                Edit
                            </Button>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="2">
                        <div class="d-grid gap-2">
                            <Button variant="danger">Delete</Button>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item eventKey="3">
                        <div class="d-grid gap-2">
                            <Button variant="info">Detail</Button>
                        </div>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        );
    };

    return (
        <Card className="h-100 border">
            <Card.Header className="bg-white py-4">
                <Row>
                    <Col lg={3} md={3} xs={3}>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <DateRangePicker placeholder="Select Date Range" />
                                </Col>
                            </Form.Group>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <Select options={options} />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col lg={3} md={3} xs={3}>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <Form.Control type="text" placeholder="Cari" />
                                </Col>
                            </Form.Group>
                            <Form.Group
                                as={Row}
                                className="mb-1 pt-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={12}>
                                    <Form.Check
                                        type="checkbox"
                                        id="autoSizingCheck[1]"
                                        label="Belum Dibaca"
                                    />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col lg={4} md={4} xs={4}>
                        <Form>
                            <Form.Group
                                as={Row}
                                className="mb-2"
                                controlId="formHorizontalEmail"
                            >
                                <Col sm={6}>
                                    <Button variant="primary" onClick={handleShow}>
                                        Komplain
                                    </Button>

                                    <Modal show={show} onHide={handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Tulis Komplain Anda</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <Form>
                                                <Form.Group
                                                    className="mb-2"
                                                    controlId="exampleForm.ControlInput1"
                                                >
                                                    <Form.Label>ID Reseller</Form.Label>
                                                    <Form.Control type="text" placeholder="IDRS-001" />
                                                </Form.Group>
                                                <Form.Group
                                                    className="mb-2"
                                                    controlId="exampleForm.ControlInput2"
                                                >
                                                    <Form.Label>Nama Reseller</Form.Label>
                                                    <Select options={options} />
                                                </Form.Group>
                                                <Form.Group
                                                    className="mb-2"
                                                    controlId="exampleForm.ControlInput1"
                                                >
                                                    <Form.Label>Nomor Pengirim</Form.Label>
                                                    <Form.Control type="text" placeholder="NO-010" />
                                                </Form.Group>
                                                <Form.Group
                                                    className="mb-2"
                                                    controlId="exampleForm.ControlTextarea1"
                                                >
                                                    <Form.Label>Isi Pesan</Form.Label>
                                                    <Form.Control as="textarea" rows={3} />
                                                </Form.Group>
                                                <Form.Group
                                                    className="mb-2"
                                                    controlId="exampleForm.ControlTextarea1"
                                                >
                                                    <Form.Label>Balasan</Form.Label>
                                                    <Form.Control as="textarea" rows={3} />
                                                </Form.Group>
                                            </Form>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={handleClose}>
                                                Batal
                                            </Button>
                                            <Button variant="primary" onClick={handleClose}>
                                                Kirim Komplain
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </Col>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
            </Card.Header>
            <Table responsive hover className="text-nowrap">
                <thead className="table-light">
                    <tr>
                        <th>IdTrx</th>
                        <th>Waktu</th>
                        <th>Nama</th>
                        <th>Tujuan</th>
                        <th>Status</th>
                        <th>Isi Komplain</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {KomplainsData.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td className="align-middle">{item.idtrx}</td>
                                <td className="align-middle">{item.waktu}</td>
                                <td className="align-middle">{item.nama}</td>
                                <td className="align-middle">{item.tujuan}</td>
                                <td className="align-middle">{item.status}</td>
                                <td className="align-middle">{item.isikomplain}</td>
                                <td className="align-middle">
                                    <ActionMenu />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </Card>
    );
};

export default Senders;
