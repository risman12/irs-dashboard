// import node module libraries
import React from "react";
import { ListGroup } from "react-bootstrap";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import "rsuite/dist/rsuite-no-reset.min.css";

const LogTerminals = () => {
  return (
    <ListGroup defaultActiveKey="#link1" className="list-terminal">
      <ListGroup.Item action href="#link1">
        Center SMS
      </ListGroup.Item>
      <ListGroup.Item action href="#link2">
        Sender SMS
      </ListGroup.Item>
      <ListGroup.Item action href="#link3">
        Center SMS & Sender SMS
      </ListGroup.Item>
      <ListGroup.Item action href="#link4">
        QPAY
      </ListGroup.Item>
      <ListGroup.Item action href="#link5">
        IRS Market
      </ListGroup.Item>
      <ListGroup.Item action href="#link6">
        ISIMPEL
      </ListGroup.Item>
      <ListGroup.Item action href="#link7">
        SIDOMPUL
      </ListGroup.Item>
      <ListGroup.Item action href="#link8">
        DIGIPOS
      </ListGroup.Item>
      <ListGroup.Item action href="#link9">
        Telegram
      </ListGroup.Item>
      <ListGroup.Item action href="#link10">
        Center Jabber
      </ListGroup.Item>
      <ListGroup.Item action href="#link11">
        Center Gmail
      </ListGroup.Item>
      <ListGroup.Item action href="#link12">
        Center Whatsapp
      </ListGroup.Item>
    </ListGroup>
  );
};

export default LogTerminals;
