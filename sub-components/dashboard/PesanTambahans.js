// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Modal,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import PesanTambahansData from "data/dashboard/PesanTambahansData";
import "rsuite/dist/rsuite-no-reset.min.css";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Modal heading
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Centered Modal</h4>
        <p>
          Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
          dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac
          consectetur ac, vestibulum at eros.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

const PesanTambahans = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="h-100 border">
      <Card.Header className="bg-white py-4">
        <Row>
          <Col lg={3} md={3} xs={3}>
            <Form>
              <Form.Group
                as={Row}
                className="mb-2"
                controlId="formHorizontalEmail"
              >
                <Col sm={12}>
                  <Button variant="primary" onClick={handleShow}>
                    Tambah
                  </Button>
                  {/* Modal */}
                  <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                      <Modal.Title>Tambah Pesan</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Form.Group
                          className="mb-2"
                          controlId="exampleForm.ControlTextarea1"
                        >
                          <Form.Label>Pesan</Form.Label>
                          <Form.Control as="textarea" rows={3} />
                        </Form.Group>
                      </Form>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                        Batal
                      </Button>
                      <Button variant="primary" onClick={handleClose}>
                        Simpan
                      </Button>
                    </Modal.Footer>
                  </Modal>
                </Col>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </Card.Header>
      <Table responsive hover className="text-nowrap">
        <thead className="table-light">
          <tr>
            <th>Pesan</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {PesanTambahansData.map((item, index) => {
            return (
              <tr key={index}>
                <td className="align-middle">{item.pesan}</td>
                <td className="align-middle">
                  <ActionMenu />
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Card>
  );
};

export default PesanTambahans;
