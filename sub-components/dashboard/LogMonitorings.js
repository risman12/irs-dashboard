// import node module libraries
import React from "react";
import Link from "next/link";
import {
  Row,
  Col,
  Card,
  Form,
  Table,
  Tab,
  Tabs,
  Button,
  Dropdown,
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
import LogMonitoringsData from "data/dashboard/LogMonitoringsData";
import "rsuite/dist/rsuite-no-reset.min.css";
import { DateRangePicker } from "rsuite";

const LogMonitorings = () => {
  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <Card className="border-0 h-100">
      <Tabs
        defaultActiveKey="home"
        id="uncontrolled-tab-example"
        className="tabs-custome"
      >
        <Tab eventKey="home" title="Home" className="border border-top-0 rounded-bottom p-4">
          <Button variant="primary">Cek Saldo</Button>
        </Tab>
        <Tab eventKey="inbox" title="Inbox" className="border border-top-0 rounded-bottom p-4">
          <Row>
            <Col lg={4} md={4} xs={4}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <DateRangePicker placeholder="Select Date Range" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Group
                      as={Row}
                      className="mb-2"
                      controlId="formHorizontalEmail"
                    >
                      <Col sm={12}>
                        <Form.Control type="text" placeholder="Pengirim" />
                      </Col>
                    </Form.Group>
                  </Col>
                </Form.Group>
              </Form>
            </Col>
            <Col lg={4} md={4} xs={4}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Control type="text" placeholder="Pesan" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Button variant="primary">Cari</Button>
                  </Col>
                </Form.Group>
              </Form>
            </Col>
          </Row>

          <Table responsive hover className="text-nowrap mt-3">
            <thead className="table-light">
              <tr>
                <th>Waktu</th>
                <th>Pengirim</th>
                <th>Pesan</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {LogMonitoringsData.map((item, index) => {
                return (
                  <tr key={index}>
                    <td className="align-middle">{item.waktu}</td>
                    <td className="align-middle">{item.pengirim}</td>
                    <td className="align-middle">{item.pesan}</td>
                    <td className="align-middle">
                      <ActionMenu />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Tab>
        <Tab eventKey="outbox" title="Outbox" className="border border-top-0 rounded-bottom p-4">
          <Row>
            <Col lg={4} md={4} xs={4}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <DateRangePicker placeholder="Select Date Range" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Group
                      as={Row}
                      className="mb-2"
                      controlId="formHorizontalEmail"
                    >
                      <Col sm={12}>
                        <Form.Control type="text" placeholder="Pengirim" />
                      </Col>
                    </Form.Group>
                  </Col>
                </Form.Group>
              </Form>
            </Col>
            <Col lg={4} md={4} xs={4}>
              <Form>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Form.Control type="text" placeholder="Pesan" />
                  </Col>
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-2"
                  controlId="formHorizontalEmail"
                >
                  <Col sm={12}>
                    <Button variant="primary">Cari</Button>
                  </Col>
                </Form.Group>
              </Form>
            </Col>
          </Row>

          <Table responsive hover className="text-nowrap mt-3">
            <thead className="table-light">
              <tr>
                <th>Waktu</th>
                <th>Pengirim</th>
                <th>Pesan</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {LogMonitoringsData.map((item, index) => {
                return (
                  <tr key={index}>
                    <td className="align-middle">{item.waktu}</td>
                    <td className="align-middle">{item.pengirim}</td>
                    <td className="align-middle">{item.pesan}</td>
                    <td className="align-middle">
                      <ActionMenu />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Tab>
      </Tabs>
    </Card>
  );
};

export default LogMonitorings;
