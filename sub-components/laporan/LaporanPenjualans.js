// import node module libraries
import React, { useState } from "react";
import Link from "next/link";
import {
  Card,
  Table,
  Dropdown,
  Row,
  Col,
  Form,
  Button,
  Alert,
  Tabs,
  Tab
} from "react-bootstrap";
import { MoreVertical } from "react-feather";

// import theme style scss file
import "styles/theme.scss";
import "styles/theme/components/_teams.scss";

// import required data files
// import LaporanPenjualansData from "data/penjualan/LaporanPenjualansData";
import "rsuite/dist/rsuite-no-reset.min.css";

import { DateRangePicker } from "rsuite";
import HistoryResellersData from "data/administrator/HistoryResellersData";

const LaporanPenjualans = () => {

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <Link
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
      className="text-muted text-primary-hover"
    >
      {children}
    </Link>
  ));

  CustomToggle.displayName = "CustomToggle";

  const ActionMenu = () => {
    return (
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle}>
          <MoreVertical size="15px" className="text-muted" />
        </Dropdown.Toggle>
        <Dropdown.Menu className="action" align={"end"}>
          <Dropdown.Item eventKey="1">
            <div class="d-grid gap-2">
              <Button variant="warning" className="d-block">
                Edit
              </Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2">
            <div class="d-grid gap-2">
              <Button variant="danger">Delete</Button>
            </div>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3">
            <div class="d-grid gap-2">
              <Button variant="info">Detail</Button>
            </div>
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  return (
    <>
      <Card>
        <Card.Body className="border rounded">
          <Form>
            <Row className="align-items-center">
              <Col xs="auto">
                <DateRangePicker placeholder="Select Date Range" />
              </Col>
              <Col xs="auto">
                <Form.Check
                  type="checkbox"
                  id="autoSizingCheck"
                  className="mt-1"
                  label="Data History"
                />
              </Col>
              <Col xs="auto">
                <Button variant="primary">Export</Button>
              </Col>
            </Row>
          </Form>
        </Card.Body>
      </Card>
      <Tabs
        defaultActiveKey="perproduk"
        id="noanim-tab-example"
        className="tabs-custome mt-3"
      >
        <Tab eventKey="perproduk" title="PerProduk" className="border border-top-0 rounded-bottom p-4">
          <Table responsive hover className="text-nowrap mb-0">
            <thead className="table-light">
              <tr>
                <th>ID</th>
                <th>Nama Operator</th>
                <th>Nama Produk</th>
                <th>Kode Produk</th>
                <th>Belum Proses</th>
                <th>Gagal</th>
                <th>Proses</th>
                <th>Sending Fail</th>
                <th>Sukses</th>
                <th>Total</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </Table>
        </Tab>
        <Tab eventKey="perterminal" title="PerTerminal" className="border border-top-0 rounded-bottom p-4">
          <Table responsive hover className="text-nowrap mb-0">
            <thead className="table-light">
              <tr>
                <th>ID</th>
                <th>Nama Supplier</th>
                <th>Nama Terminal</th>
                <th>Kode Operator</th>
                <th>Kode Produk</th>
                <th>Sukses</th>
                <th>Harga Beli</th>
                <th>Harga Jual</th>
                <th>Selisih</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </Table>
        </Tab>
        <Tab eventKey="labarugi" title="Laba Rugi Transaksi" className="border border-top-0 rounded-bottom p-4">
          <Form as={Row}>
            <Col sm={6} md={6} lg={6}>
              <Form.Group as={Row} className="mb-3" controlId="formHorizontalText">
                <Form.Label column sm={2}>
                  Omset
                </Form.Label>
                <Col sm={10}>
                  <Form.Control type="text" placeholder="" />
                </Col>
              </Form.Group>

              <Form.Group as={Row} className="mb-3" controlId="formHorizontalText">
                <Form.Label column sm={2}>
                  Penjualan
                </Form.Label>
                <Col sm={10}>
                  <Form.Control type="text" placeholder="" />
                </Col>
              </Form.Group>
              <Form.Group as={Row} className="mb-3" controlId="formHorizontalText">
                <Form.Label column sm={2}>
                  Pembelian
                </Form.Label>
                <Col sm={10}>
                  <Form.Control type="text" placeholder="" />
                </Col>
              </Form.Group>

              <Form.Group as={Row} className="mb-3" controlId="formHorizontalText">
                <Form.Label column sm={2}>
                  Laba Rugi
                </Form.Label>
                <Col sm={10}>
                  <Form.Control type="text" placeholder="" />
                </Col>
              </Form.Group>
            </Col>
          </Form>
        </Tab>
        <Tab eventKey="rekap" title="Rekap PerAgen" className="border border-top-0 rounded-bottom p-4">
          <Row>
            <Col xs={4} sm={4} md={4} lg={4}>
              <Table responsive hover className="text-nowrap mb-0">
                <thead className="table-light">
                  <tr>
                    <th>Grand Total</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </Table>
            </Col>
          </Row>
        </Tab>
        <Tab eventKey="termcluster" title="Terminal Cluster" className="border border-top-0 rounded-bottom p-4">
          <Table responsive hover className="text-nowrap mb-0">
            <thead className="table-light">
              <tr>
                <th>ID</th>
                <th>Cluster</th>
                <th>Terminal</th>
                <th>Nominal</th>
                <th>Sukses</th>
                <th>Sukses</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </Table>
        </Tab>
        <Tab eventKey="pph" title="Pajak PPH" className="border border-top-0 rounded-bottom p-4">
          <fieldset>
            <Form.Group as={Row} className="mb-3">
              <Col sm={12}>
                <Form.Check
                  inline
                  type="radio"
                  label="PPH22"
                  name="formHorizontalRadios"
                  id="formHorizontalRadios1"
                />
                <Form.Check
                  inline
                  type="radio"
                  label="PPH23"
                  name="formHorizontalRadios"
                  id="formHorizontalRadios1"
                />
              </Col>
            </Form.Group>
          </fieldset>
          <Row>
            <Col xs={4} sm={4} md={4} lg={4}>

              <Table responsive hover className="text-nowrap mb-0">
                <thead className="table-light">
                  <tr>
                    <th>Grand Total</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </Table>
            </Col>
          </Row>
        </Tab>
      </Tabs>
    </>
  );
};

export default LaporanPenjualans;
