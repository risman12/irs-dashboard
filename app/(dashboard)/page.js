"use client";
// import node module libraries
import { Fragment } from "react";
import Link from "next/link";
import { Container, Col, Row, } from "react-bootstrap";

// import widget/custom components
import { StatRightTopIcon } from "widgets";

// import sub components
import {
  ActiveProjects,
  Transaksis,
} from "sub-components";

const Home = () => {
  return (
    <Fragment>
      <div className="bg-transparent pt-8 pb-21"></div>
      <Container fluid className="mt-n22 px-6">
        <Transaksis />

        {/* <Row className="my-6">
          <Col xl={12} lg={12} md={12} xs={12}>
            <ActiveProjects />
          </Col>
        </Row> */}
      </Container>
    </Fragment>
  );
};
export default Home;
