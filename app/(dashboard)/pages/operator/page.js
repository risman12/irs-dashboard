"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { Operators } from "sub-components";
import { Areas } from "sub-components";

const Operator = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        {/* card  */}
        <Col xl={6} lg={6} md={6} xs={6}>
          <Operators />
        </Col>

        <Col xl={6} lg={6} md={6} xs={6}>
          <Areas />
        </Col>
      </Row>
    </Container>
  );
};

export default Operator;
