"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { Senders } from "sub-components";

const Sender = () => {
  return (
    <Container fluid className="px-6">
      {/* Form Transaction */}
      <Row>
        {/* card  */}
        <Col xl={12} lg={12} md={12} xs={12}>
          {/* Teams  */}
          <Senders />
        </Col>
      </Row>
    </Container>
  );
};

export default Sender;
