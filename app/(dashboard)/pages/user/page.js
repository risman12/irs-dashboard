"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { Users } from "sub-components";

const User = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        {/* card  */}
        <Col xl={12} lg={12} md={12} xs={12}>
          <Users />
        </Col>
      </Row>
    </Container>
  );
};

export default User;
