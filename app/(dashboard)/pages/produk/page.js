"use client";
// import node module libraries
import { Col, Row, Container, Tabs, Tab } from "react-bootstrap";

// import sub components
import { Produks } from "sub-components";
import { PricePlans } from "sub-components";

const Produk = () => {
  return (
    <Container fluid className="px-6">
      <Tabs
        defaultActiveKey="produk"
        transition={false}
        id="noanim-tab-example"
        className="tabs-custome"
      >
        <Tab eventKey="produk" title="Produk" className="border border-top-0 rounded-bottom p-4">
          <Produks />
        </Tab>
        <Tab eventKey="priceplan" title="Price Plan" className="border border-top-0 rounded-bottom p-4">
          <PricePlans />
        </Tab>
      </Tabs>
    </Container>
  );
};

export default Produk;
