"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { OptionsSystem, OptionsBisnis } from "sub-components";

const Option = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        {/* card  */}
        <Col xl={8} lg={8} md={8} xs={8}>
          <OptionsSystem />
        </Col>
        <Col xl={4} lg={4} md={4} xs={4}>
          <OptionsBisnis />
        </Col>
      </Row>
    </Container>
  );
};

export default Option;
