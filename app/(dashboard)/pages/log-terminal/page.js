"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { LogTerminals } from "sub-components";
import { LogMonitorings } from "sub-components";

const LogTerminal = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        {/* card  */}
        <Col xl={4} lg={4} md={4} xs={4}>
          <LogTerminals />
        </Col>

        <Col xl={8} lg={8} md={8} xs={8}>
          <LogMonitorings />
        </Col>
      </Row>
    </Container>
  );
};

export default LogTerminal;
