"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { Transaksis } from "sub-components";

const Transaksi = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        <Col lg={12} md={12} xs={12}>
          <div className="mb-4">
            <h3 className="mb-0 fw-bold">Transaksi</h3>
          </div>
        </Col>
      </Row>
      <Row>
        {/* card  */}
        <Col xl={12} lg={12} md={12} xs={12}>
          {/* Transaksi  */}
          <Transaksis />
        </Col>
      </Row>
    </Container>
  );
};

export default Transaksi;
