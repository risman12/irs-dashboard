"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { Inventorys } from "sub-components";

const Inventory = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        <Col xl={12} lg={12} md={12} xs={12}>
          <Inventorys />
        </Col>
      </Row>
    </Container>
  );
};

export default Inventory;
