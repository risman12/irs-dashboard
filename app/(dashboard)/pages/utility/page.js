"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { Utilitys, UtilitysBtn } from "sub-components";

const Utility = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        {/* card  */}
        <Col xl={8} lg={8} md={8} xs={8}>
          <Utilitys />
        </Col>
        <Col xl={4} lg={4} md={4} xs={4}>
          <UtilitysBtn />
        </Col>
      </Row>
    </Container>
  );
};

export default Utility;
