"use client";
// import node module libraries
import { Container, Tabs, Tab, Table } from "react-bootstrap";

// import sub components
import { VoucherFisiks } from "sub-components";

const VoucherFisik = () => {
  return (
    <Container fluid className="px-6">
      <Tabs
        defaultActiveKey="voucherfisik"
        transition={false}
        id="noanim-tab-example"
        className="tabs-custome"
      >
        <Tab eventKey="voucherfisik" title="Voucher Topup Chip" className="border border-top-0 rounded-bottom p-4">
          <VoucherFisiks />
        </Tab>
        <Tab eventKey="vouchergosok" title="Voucher Fisik Gosok" className="border border-top-0 rounded-bottom p-4">
          <Table responsive hover className="text-nowrap mb-0 mt-2">
            <thead className="table-light">
              <tr>
                <th>ID</th>
                <th>Kode Produk</th>
                <th>Nama Produk</th>
                <th>No Gosok</th>
                <th>No Seri</th>
                <th>Tanggal Expired</th>
                <th>Waktu Input</th>
              </tr>
            </thead>
            <tbody></tbody>
          </Table>
        </Tab>
        <Tab eventKey="voucherterjual" title="Voucher Fisik Terjual" className="border border-top-0 rounded-bottom p-4">
          <Table responsive hover className="text-nowrap mb-0 mt-2">
            <thead className="table-light">
              <tr>
                <th>ID Trx</th>
                <th>Waktu Keluar</th>
                <th>Kode Produk</th>
                <th>No Seri</th>
                <th>Tujuan</th>
                <th>ID Reseller</th>
                <th>Nama Reseller</th>
              </tr>
            </thead>
            <tbody></tbody>
          </Table>
        </Tab>
      </Tabs>
    </Container>
  );
};

export default VoucherFisik;
