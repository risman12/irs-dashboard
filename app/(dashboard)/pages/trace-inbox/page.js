"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { TraceInboxs } from "sub-components";

const TraceInbox = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        <Col xl={12} lg={12} md={12} xs={12}>
          <TraceInboxs />
        </Col>
      </Row>
    </Container>
  );
};

export default TraceInbox;
