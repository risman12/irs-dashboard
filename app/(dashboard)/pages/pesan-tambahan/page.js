"use client";
// import node module libraries
import { Col, Row, Container } from "react-bootstrap";

// import sub components
import { PesanTambahans } from "sub-components";

const PesanTambahan = () => {
  return (
    <Container fluid className="px-6">
      <Row>
        {/* card  */}
        <Col xl={12} lg={12} md={12} xs={12}>
          <PesanTambahans />
        </Col>
      </Row>
    </Container>
  );
};

export default PesanTambahan;
