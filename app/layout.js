// import theme style scss file
import "styles/theme.scss";

export const metadata = {
    title: "IRS DASHBOARD",
};

export default function RootLayout({ children }) {
    return (
        <html lang="en">
            <body className="bg-white">{children}</body>
        </html>
    );
}
