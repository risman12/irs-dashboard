export const ClustersData = [
    {
        id: 1,
        nama: "Batang",
        namalengkap: "Batang Kara",
        akses: "Owner",
    },
    {
        id: 2,
        nama: "Bulan",
        namalengkap: "Bulan Bintang",
        akses: "Owner",
    },
    {
        id: 3,
        nama: "Jomang",
        namalengkap: "Jomang Khan",
        akses: "Owner",
    },
    {
        id: 4,
        nama: "Longan",
        namalengkap: "Longan aerialan",
        akses: "Owner",
    },
    {
        id: 5,
        nama: "Dorwin",
        namalengkap: "Dorwin Sched",
        akses: "Owner",
    },
];
export default ClustersData;
