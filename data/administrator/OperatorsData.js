export const OperatorsData = [
  {
    id: 1,
    group: "Indosat",
    mindigit: "10",
    maxdigit: "13",
  },
  {
    id: 2,
    group: "Telkomsel",
    mindigit: "10",
    maxdigit: "13",
  },
  {
    id: 3,
    group: "Axis",
    mindigit: "10",
    maxdigit: "13",
  },
  {
    id: 4,
    group: "Smartfren",
    mindigit: "10",
    maxdigit: "13",
  },
  {
    id: 5,
    group: "XL",
    mindigit: "10",
    maxdigit: "13",
  },
  {
    id: 6,
    group: "Three",
    mindigit: "10",
    maxdigit: "13",
  },
  {
    id: 7,
    group: "MKIOS",
    mindigit: "10",
    maxdigit: "13",
  },
];
export default OperatorsData;
