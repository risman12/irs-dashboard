export const ClustersData = [
    {
        id: 1,
        cluster: "Jakarta",
    },
    {
        id: 2,
        cluster: "Bogor",
    },
    {
        id: 3,
        cluster: "Depok",
    },
    {
        id: 4,
        cluster: "Tangerang",
    },
    {
        id: 5,
        cluster: "Bekasi",
    },
    {
        id: 6,
        cluster: "Bandung",
    },

];
export default ClustersData;
