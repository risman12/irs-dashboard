export const SendersData = [
  {
    id: 1,
    namaarea: "Bali Nursa",
  },
  {
    id: 2,
    namaarea: "Jakarta",
  },
  {
    id: 3,
    namaarea: "Maluku",
  },
  {
    id: 4,
    namaarea: "Aceh",
  },
  {
    id: 5,
    namaarea: "Balikpapan",
  },
  {
    id: 6,
    namaarea: "Bandung",
  },
  {
    id: 7,
    namaarea: "Papua",
  },
  {
    id: 8,
    namaarea: "Palembang",
  },
  {
    id: 9,
    namaarea: "Sulawesi",
  },
];
export default SendersData;
