const Notification = [
	{
		id: 1,
		sender: 'Rishi Chopra',
		message: `Mauris blandit erat id nunc blandit, ac eleifend dolor pretium.`
	},
	{
		id: 2,
		sender: 'Neha Kannned',
		message: `Proin at elit vel est condimentum elementum id in ante. Maecenas et sapien metus.`
	}
];

export default Notification;
